#Temperature Conversion Functions

def ftoc(deg_f)
    (deg_f.to_f - 32.0) * (5.0 / 9.0)
end

def ctof(deg_c)
    (deg_c.to_f * (9.0 / 5.0)) + 32
end