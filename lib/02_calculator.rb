def add(int_1, int_2)
    int_1 + int_2
end

def subtract(int_1, int_2)
    int_1 - int_2
end

def sum(arr)
    if arr.length == 0
        return 0
    else
        arr.reduce(:+)
    end
end

def multiply(*ints)
   ints.reduce(:*) 
end

def power(int_1, int_2)
   int_1**int_2 
end

def factorial(num)
    if num == 0
        return 1
    elsif num == 1
        num
    else
      
      factorial(num - 1) * num
    end
end