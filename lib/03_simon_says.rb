def echo(str)
   str 
end
    
def shout(str)
    str.upcase
end

def repeat(str, num=2)
    count = 0
    final_str = ""
    
    while count < num - 1
        
        final_str += str + " "
        
        count += 1
    end
    if count == num - 1
        final_str += str
    end
    
    final_str
end

def start_of_word(str, num)
    final = []
    arr = str.chars
    num.times { final << arr.shift }
    final.join
end

def first_word(str)
    arr = str.split
    arr[0]
end

def titleize(str)
    #new_str = ""
  a_str =  str.split.map! do |word|
             if !little_word?(word)
               word.capitalize!
             else
               word
             end
        end
    a_str[0].capitalize!
    a_str.join(" ")
end

private

def little_word?(word)
    little_words = ["and", "or", "over", "the", "at", "of"]
    little_words.include?(word)
end